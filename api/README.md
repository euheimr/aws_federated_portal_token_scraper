### Using aws_cred.py
* Python 3.6+ required.

* Only Federated logins using MFA are supported.

  
aws_cred.py can be called from the command line.
  ```aws_cred.py ACCOUNT_ID DOMAIN_URL EXTERNAL_ID MFA_ARN MFA_TOKEN ROLE_NAME DURATION```
    Example:
        ```$ aws_cred.py 123456789012 example.org DoeJ MFA_ARN MFA_TOKEN ROLE_NAME DURATION```
    
Calling the function ```cli( ... )``` from aws_cred will: 
* Assume role with MFA using STS by generating credentials for a default duration of 1 hour
* Generate a sign-in token with IAM using those temporary credentials and constructed Federated domain URL
* Returns a signed Federated URL
* Returns False if there's any failure.


Completing an AWS STS Federated MFA login using a python script (do_login.py) calling aws_cred would be alike to the example below.
    
    filename: do_login.py
    > do_login.py account_id 
```python
import getpass
import sys
import aws_cred 
# import aws_cred.py from the same directory as the calling script 

# An idea is to pass the script through the command line
# sys.argv is a list in Python, which contains the command-line 
#   arguments passed to the script.

# sys.argv[0] is the name of the script being called (do_login.py)
account_id = sys.argv[1]  # Account id of target account
role_name = sys.argv[2]  # Name of role to assume in target account
mfa_arn = sys.argv[3]

duration = 3600  # 1 Hour
domain_url = 'Example.org'  # domain_url name for signin url

# Prompt for an MFA time-based one-time password (token)
external_id = getpass.getpass('External ID: ')
mfa_token = getpass.getpass('MFA Token: ')
     
try:
    url = aws_cred.cli(account_id, mfa_arn, domain_url, 
              external_id, mfa_token, duration, role_name)
    print(url)
except Exception as e:
    print('Received error: ', e)
    sys.exit(1)
```

### Amazon Resource Name (ARN) URL structure
IAM ARNs

Most resources have a friendly name (for example, a user named Bob or a group named Developers). However, the permissions policy language requires you to specify the resource or resources using the following Amazon Resource Name (ARN) format.

arn:partition:service:region:account:resource

Where:

* ```partition``` identifies the partition that the resource is in. For standard AWS Regions, the partition is aws. If you have resources in other partitions, the partition is aws-partitionname. For example, the partition for resources in the China (Beijing) Region is aws-cn. You cannot delegate access between accounts in different partitions.

* ```service``` identifies the AWS product. For IAM resources, this is always iam.

* ```region``` is the Region the resource resides in. For IAM resources, this is always kept blank.

* ```account``` is the AWS account ID with no hyphens (for example, 123456789012).

* ```resource``` is the portion that identifies the specific resource by name.

You can specify IAM and AWS STS ARNs using the following syntax. The Region portion of the ARN is blank because IAM resources are global. 
#### AWS STS
A federated user identified in IAM as "Paulo":
```arn:aws:sts::123456789012:federated-user/Paulo```

#### AWS IAM
An IAM user in the account:
```arn:aws:iam::123456789012:user/JohnDoe```

Another user with a path reflecting an organization chart:
```arn:aws:iam::123456789012:user/division_abc/subdivision_xyz/JaneDoe```

An IAM group:
```arn:aws:iam::123456789012:group/Developers```

An IAM group with a path:
```arn:aws:iam::123456789012:group/division_abc/subdivision_xyz/product_A/Developers```

An IAM role:
```arn:aws:iam::123456789012:role/S3Access```

A managed policy:
```arn:aws:iam::123456789012:policy/ManageCredentialsPermissions```

An instance profile that can be associated with an EC2 instance:
```arn:aws:iam::123456789012:instance-profile/Webserver```
