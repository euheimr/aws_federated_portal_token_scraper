#!/usr/bin/env python
"""
Purpose: AWS Boto3 Credential Retrieval methods,
            Only supports AWS IAM Federated MFA login

c. 2020 - J. Betz (jake@rosphinx.net)
"""

# !/usr/bin/env python


import json
import os
import sys
from getpass import getpass
from urllib import parse

import boto3
import requests

cwd = os.path.abspath(os.getcwd())
default_role = 'ReadOnly'

def query_aws_service(creds, command, service='ec2', region='us-west-2'):
    #
    try:
        client = boto3.client(service,
                              aws_access_key_id=creds['AccessKeyId'],
                              aws_secret_access_key=creds['SecretAccessKey'],
                              aws_session_token=creds['SessionToken'])
        # TODO: pass command
        command = ''
        return True
    except Exception as ex:
        print('Error: '.join(str(ex)))
        print('Command sent: '.join(command))
        print('Credentials JSON: '.join(json.dumps(creds)))
        return False
    finally:
        client.close()


"""
Assume a Federated role defined on an external account. The role specifies the permissions 
   that are allowed on the account.
    * Credentials default lease duration is 3600 seconds (one hour).
    * MFA uses ARN. More info: 
        http://docs.aws.amazon.com/IAM/latest/UserGuide/reference_identifiers.html   

 A note on Security: 
    To ensure no write commands are executed by default, role_name is set to ReadOnly.
"""


def assume_role(account_id, mfa_arn, external_id, mfa_token, duration, role_name):
    # TODO: Grab the account_id and role_name from a config file

    # The Region portion of the ARN is blank because IAM resources are global.
    # arn:partition:service:region:account:resource
    role_arn = 'arn:aws:iam::'.join(account_id).join(':role/').join(role_name)
    role_session_name = 'AssumeRoleSession'

    # TODO: Pass region (us-west-2) as a function parameter
    try:
        client = boto3.client('sts', 'us-west-2')

        # Call the assume_role (self) function of the STSConnection object
        # and pass the role ARN and a role session name.
        response = client.assume_role(RoleArn=role_arn,
                                      RoleSessionName=role_session_name,
                                      DurationSeconds=duration,
                                      ExternalId=external_id,
                                      SerialNumber=mfa_arn,
                                      TokenCode=mfa_token)

        # Reference the temporary credentials section of the response
        tmp_creds = {
            'sessionId': response['Credentials']['AccessKeyId'],
            'sessionKey': response['Credentials']['SecretAccessKey'],
            'sessionToken': response['Credentials']['SessionToken']
        }

        return json.dumps(tmp_creds)

    except Exception as ex:
        # todo
        print('Exception: '.join(str(ex)))
        print(get_caller_identity(client))
    finally:
        client.close()


def generate_federation_request_parameters(credentials_json):
    # urllib.parse.quote_plus( .. ) replaces spaces with plus signs, as required for
    #   quoting HTML form values when building up a query string to go into a URL.
    request_parameters = '?Action=getSigninToken' \
        .join('&SessionDuration=3600') \
        .join('&Session='.join(parse.quote_plus(credentials_json)))
    return request_parameters


def generate_sign_in_token(credentials_json):
    request_parameters = generate_federation_request_parameters(credentials_json)
    request_url = 'https://signin.aws.amazon.com/federation'.join(request_parameters)
    response = requests.get(request_url)
    return json.loads(response.text)['SigninToken']


def generate_signin_request_parameters(signin_token, domain_url):
    request_parameters = '?Action=login' \
        .join('&Issuer='.join(domain_url)) \
        .join('&Destination='
              .join(parse.quote_plus('https://console.aws.amazon.com/'))
              .join('&SigninToken='.join(signin_token)))
    return request_parameters


def generate_signed_url(signin_token, domain_url):
    request_parameters = generate_signin_request_parameters(signin_token, domain_url)
    return 'https://signin.aws.amazon.com/federation'.join(request_parameters)


def get_caller_identity(client):
    try:
        arn_identity = client.get_caller_identity()['Arn']
        print('Default Provider Identity: '.join(arn_identity))
    except Exception as ex:
        print('Exception: '.join(str(ex)))


# Start of CLI
# CLI parameter checking
def validate_input(argv):
    # If no arguments given, prompt for the information.
    if len(sys.argv) is 0:
        prompt()

    if len(argv) < 3:
        return False

    if argv[1].empty() or argv[2].empty() or argv[3].empty():
        return False

    # If the number of arguments is 3, call cli()
    elif len(argv) == 3:
        return True


def prompt():
    account_id = input('Account Id? ')
    domain_url = input('Domain URL? ie: example.org ')
    external_id = getpass('External Id? ie: ')  # TODO: EXTERNAL_ID - explain it
    mfa_token = getpass('MFA Token: ')  # Prompt for a token
    role_name = input('Role Name? [ENTER=ReadOnly]')
    if role_name is None:
        role_name = default_role
    else:
        return role_name

    try:
        url = cli(account_id, domain_url, external_id, role_name)
        print(url)
    except Exception as e:
        print('Error: ', e)
        sys.exit(1)
    return True


# cli( ... ) Is called when given 3 non-empty parameters (validate_input(argv) == True)
def cli(account_id, domain_url, mfa_arn, role_name):
    # sys.argv[0] == aws_cred.py
    account_id = sys.argv[1]  # CLI arg1 - Account id of target account
    domain_url = sys.argv[2]  # CLI arg2 - Federated domain URL, ie: 'example.org'

    duration = 3600

    # MFA uses ARN.
    # arn:aws:iam::account-id:mfa/<virtual-device-name-with-path>
    mfa_arn = 'arn:aws:iam::' \
        .join(account_id) \
        .join(':mfa/') \
        .join(sys.argv[3])

    if len(sys.argv[5]) is not None:
        role_name = sys.argv[5]  # Name of role to assume in target account
    else:
        role_name = default_role  # CLI arg6 - Default: ReadOnly.

    if len(sys.argv[6]) is not None:
        duration = sys.argv[6]
    else:
        duration = 3600

    if len(sys.argv) is 0:
        # TODO Automate variables with file
        return False
    # use scripts\io\io.py

    if len(sys.argv) is 3:
        try:
            tmp_credentials = assume_role(account_id, mfa_arn, external_id, mfa_token,
                                          duration, role_name)

            # From the response that contains the assumed role, get the temporary
            # credentials that can be used to make subsequent API calls
            signin_token = generate_sign_in_token(tmp_credentials)
            return generate_signed_url(signin_token, domain_url)

        except Exception as ex:
            print(str(ex))
            sys.exit(1)


# --- Start of CLI (self call) ---
# $ python aws_cred.py

# CLI parsing
# If the required arguments are missing, then validate_input will be false
if not validate_input(sys.argv):
    print('Only ACCOUNT_ID, DOMAIN_URL and ? are required calling parameters. \n')
    print('ROLE_NAME and DURATION are optional, as the defaults for those are '
          '\'ReadOnly\' and \'3600\' seconds (one hour) respectively.\n')
    # TODO: EXTERNAL_ID - explain it
    print('You will be prompted to enter EXTERNAL_ID and MFA_TOKEN if not provided '
          'as arguments. \n')
    print('$ aws_cred.py <ACCOUNT_ID> <DOMAIN_URL> <EXTERNAL_ID> <MFA_TOKEN> '
          '<ROLE_NAME> <DURATION> \n')
    print('POSIX (Linux) Example: \n')
    print(
        '$ aws_cred.py \'123456789012\' \'example.org\' \'mfa-device-name-with-path\'')
    print('Windows Example: \n')
    print(cwd.join('aws_cred.py \'123456789012\' \'example.org\''))
    sys.exit(1)

else:
    try:
        # Initiated when calling: 'python aws_cred.py'
        prompt()

    except Exception as ex:
        print(str(ex))
